import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import("./components/home/home.module").then(m => m.HomeModule)
  },
  {
    path: 'cadastro',
    loadChildren: () => import("./components/cadastro/cadastro.module").then(m => m.CadastroModule)
  },
  {
    path: 'carros',
    loadChildren: () => import("./components/carros/carros.module").then(m => m.CarrosModule)
  },
  {
    path: 'motos',
    loadChildren: () => import("./components/motos/motos.module").then(m => m.MotosModule)
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
