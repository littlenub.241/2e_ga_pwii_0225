import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarrosModule } from './carros.module';

const routes: Routes = [
  { path:'carros', component: CarrosModule }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarrosRoutingModule { }
