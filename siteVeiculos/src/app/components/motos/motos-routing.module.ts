import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MotosModule } from './motos.module';

const routes: Routes = [
  { path:'motos', component: MotosModule }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MotosRoutingModule { }
